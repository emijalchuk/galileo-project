var fs = require('fs');
var util = require('./util.js');

var ARCHIVO_LOG = './log.txt'

function clearSync(){
    fs.writeFileSync(ARCHIVO_LOG, '');
}

clearSync();

module.exports = {
    registerSync: function (numeroRequest, mensaje){
        var formattedMessaje = '[' + numeroRequest + '] (' + util.getFormattedDateTime(new Date(), true) + ') - ' + mensaje;
        fs.appendFileSync(ARCHIVO_LOG, formattedMessaje + '\n');
        console.log(formattedMessaje);
    }
};