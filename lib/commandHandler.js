var aLedGalileo = require('../gpio/actuadores/ledGalileo.js');
var aLedMin = require('../gpio/actuadores/ledMin.js');
var aLedMax = require('../gpio/actuadores/ledMax.js');
var aMotorMin = require('../gpio/actuadores/motorMin.js');
var aMotorMax = require('../gpio/actuadores/motorMax.js');
var commandTemperatura = require('./commandTemperatura.js');
var serviceManager = require('./serviceManager.js');

module.exports = {
    evaluar: function(command, valor){
        switch(command){
            case "encenderLedGalileo":
                aLedGalileo.encender();
                break;
            case "apagarLedGalileo":
                aLedGalileo.apagar();
                break;
            case "encenderLedMin":
                aLedMin.encender();
                break;
            case "apagarLedMin":
                aLedMin.apagar();
                break;
            case "encenderLedMax":
                aLedMax.encender();
                break;
            case "apagarLedMax":
                aLedMax.apagar();
                break;
            case "encenderMotorMin":
                aMotorMin.encender();
                break;
            case "apagarMotorMin":
                aMotorMin.apagar();
                break;
            case "encenderMotorMax":
                aMotorMax.encender();
                break;
            case "apagarMotorMax":
                aMotorMax.apagar();
                break;
            case "seleccionarTemperatura":
                commandTemperatura.setTemperaturaSeleccionada(valor);
                break;
            case "renaudarAutomatizacion":
                commandTemperatura.renaudarAutomatizacion();
                break;
            case "forzarExcepcion":
                throw "Cierre forzado mediante excepcion.";
            case "forzarCierre":
                serviceManager.finalizarServicio();
                break;
            default:
                break;
        }
    }
};
