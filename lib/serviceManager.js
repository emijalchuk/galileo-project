var log = require('./log.js');
var aLedGalileo = require('../gpio/actuadores/ledGalileo.js');
var aPantalla = require('../gpio/actuadores/pantalla.js');
var aLedMin = require('../gpio/actuadores/ledMin.js');
var aLedMax = require('../gpio/actuadores/ledMax.js');
var aMotorMin = require('../gpio/actuadores/motorMin.js');
var aMotorMax = require('../gpio/actuadores/motorMax.js');

function terminarServicio(){
    log.registerSync(-1, "Finalizando servicio...");
    aLedGalileo.apagar();
    aLedMin.apagar();
    aLedMax.apagar();
    aMotorMin.apagar();
    aMotorMax.apagar();
    aPantalla.cerrar();
    const GOODBYE_MESSAGE_TIME_SECONDS = 3;
    setTimeout(function(){
        aPantalla.borrar();
        log.registerSync(-1, "Servicio finalizado con exito.");
        process.exit(0);
    }, GOODBYE_MESSAGE_TIME_SECONDS * 1000);
}

module.exports = {
    iniciarlizarServicio: function(){
        aLedGalileo.encender();
        var finalizarServicio = this.finalizarServicio;
        process.on('uncaughtException', function(err) {
            var mensajeError = 'Proceso finalizado por excepcion no controlada:'
            mensajeError += '\n===========';
            mensajeError += '\n' + err;
            mensajeError += '\n===========';
            log.registerSync(-1, mensajeError);
            terminarServicio();
        });
    },
    finalizarServicio: function(){
        terminarServicio();
    }
};
