var fs = require('fs');
var util = require('./util');
var aLedGalileo = require('../gpio/actuadores/ledGalileo.js');
var aLedMin = require('../gpio/actuadores/ledMin.js');
var aLedMax = require('../gpio/actuadores/ledMax.js');
var aMotorMin = require('../gpio/actuadores/motorMin.js');
var aMotorMax = require('../gpio/actuadores/motorMax.js');
var aPantalla = require('../gpio/actuadores/pantalla.js');
var sTemperatura = require('../gpio/sensores/temperatura.js');
var sPresencia = require('../gpio/sensores/presencia.js');
var sPotenciometro = require('../gpio/sensores/potenciometro.js');
var commandTemperatura = require('./commandTemperatura.js');

var ARCHIVO_HTML = fs.readFileSync('index.html');

function getResponseJson(isJsonForAndroidApp){
    var theJson = {
                automatizacionDeshabilitada: commandTemperatura.getAutomatizacionDeshabilitada(),
                automatizacionDeshabilitadaSegundos: commandTemperatura.getAutomatizacionDeshabilitadaSegundos(),
                dateTimeAutomatizacionDeshabilitada: commandTemperatura.getDateTimeAutomatizacionDeshabilitada(),
                temperaturaSeleccionada: commandTemperatura.getTemperaturaSeleccionada(),
                aLedGalileo: aLedGalileo.getEstado(),
                aLedMin: aLedMin.getEstado(),
                aLedMax: aLedMax.getEstado(),
                aMotorMin: aMotorMin.getEstado(),
                aMotorMax: aMotorMax.getEstado(),
                aPantalla: aPantalla.getTemperaturas(),
                sPotenciometro: sPotenciometro.getTemperatura(),
                sPresencia: sPresencia.getEstado(),
                sTemperatura: sTemperatura.getTemperatura()
            };
    if(isJsonForAndroidApp)
        return { galileo: [theJson] };
    else
        return theJson;
}

module.exports = {
    getRequestMode: function(request){
        var mode = util.getValueOfQueryStringKey(request, 'modo');
        return mode;
    },
    getRequestCommand: function(request){
        var command = util.getValueOfQueryStringKey(request, 'cmd');
        return command;
    },
    getRequestCommandValue: function(request){
        var commandValue = util.getValueOfQueryStringKey(request, 'valor');
        return commandValue;
    },
    writeResponse: function(response, mode){
        var contenType = '';
        var body = '';
        switch(mode){
            case "json":
                contenType = 'application/json';
                var theJson = getResponseJson(true);
                body = JSON.stringify(theJson);
                break;
            default:
                contenType = 'text/html';
                body = ARCHIVO_HTML;
                var theJson = getResponseJson(false);
                var customFormmatedJson = JSON.stringify(theJson, null, '<br/>').replace('{', '').replace('}', '');
                body += '<br><br>..::JSON-RESPONSE::..' + customFormmatedJson;
                break;
        }
        response.writeHead(200, {'Content-Type': contenType});
        response.write(body);
        response.end();
    }
};