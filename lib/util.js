module.exports = {
    getValueOfQueryStringKey: function(request, key){
        key += '=';
        var indexOfStart = request.url.indexOf(key);
        var hayValue = indexOfStart !== -1;
        indexOfStart = indexOfStart + key.length;
        var urlAcotada = request.url.substr(indexOfStart);
        var indexOfEnd = urlAcotada.indexOf('&');
        indexOfEnd = indexOfEnd !== -1
            ? indexOfEnd
            : urlAcotada.length;
        var value = hayValue
            ? urlAcotada.substr(0, indexOfEnd)
            : '';
        return value;
    },
    getFormattedDateTime: function(date, informarSolamenteHorario){
        var date = new Date();
        var anio = date.getFullYear();
        var mes = date.getMonth();
        if(mes < 10) mes = '0' + mes;
        var dia = date.getDate();
        if(dia < 10) dia = '0' + dia;
        var hora = date.getHours();
        if(hora < 10) hora = '0' + hora;
        var minuto = date.getMinutes();
        if(minuto < 10) minuto = '0' + minuto;
        var segundo = date.getSeconds();
        if(segundo < 10) segundo = '0' + segundo;

        var fechaString = dia + '/' + mes + '/' + anio;
        var horarioString = hora + ':' + minuto + ':' + segundo;

        var respuesta = informarSolamenteHorario
            ? horarioString
            : fechaString + ' ' + horarioString;
        return respuesta;
    }
};
