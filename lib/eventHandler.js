var log = require('./log.js');
var eventEmitter = require('./eventEmitter.js')
var aPantalla = require('../gpio/actuadores/pantalla.js');
var aMotorMin = require('../gpio/actuadores/motorMin.js');
var aMotorMax = require('../gpio/actuadores/motorMax.js');
var aLedMin = require('../gpio/actuadores/ledMin.js');
var aLedMax = require('../gpio/actuadores/ledMax.js');

const DIFERENCIA_MINIMA = 1;
const DIFERENCIA_MAXIMA = 3;

var temperaturaSeleccionada = 0;
var temperaturaSensor = 0;
var existePresencia = false;
var automatizacionDeshabilitada = false;

function evaluarCondicion(){
    if(existePresencia || automatizacionDeshabilitada){
        var dif = Math.abs(temperaturaSeleccionada - temperaturaSensor);
        var cotaInfTemperatura = temperaturaSensor - DIFERENCIA_MINIMA;
        var haceCalor = temperaturaSeleccionada < cotaInfTemperatura;
        if(haceCalor){
            aLedMin.apagar();
            aLedMax.apagar();
            aMotorMin.encender();
            if(dif > DIFERENCIA_MAXIMA){
                aMotorMax.encender();
            }else{
                aMotorMax.apagar();
            }
        }else{
            var cotaSupTemperatura = temperaturaSensor + DIFERENCIA_MINIMA;
            var haceFrio = temperaturaSeleccionada > cotaSupTemperatura;
            if(haceFrio){
                aMotorMin.apagar();
                aMotorMax.apagar();
                aLedMin.encender();
                if(dif > DIFERENCIA_MAXIMA){
                    aLedMax.encender();
                }else{
                    aLedMax.apagar();
                }
            }else{
                aMotorMin.apagar();
                aMotorMax.apagar();
                aLedMin.apagar();
                aLedMax.apagar();
            }
        }
    }else{
        aLedMin.apagar();
        aLedMax.apagar();
        aMotorMin.apagar();
        aMotorMax.apagar();
    }
}

function eventTemperaturaSeleccionadaWeb(temperatura){
    log.registerSync(-1, 'eventTemperaturaSeleccionadaWeb: "' + temperatura + '".')
    temperaturaSeleccionada = temperatura;
    aPantalla.setTemperaturaSeleccionada(temperatura);
    automatizacionDeshabilitada = true;
    evaluarCondicion();
}

function eventRenaudarAutomatizacion(){
    log.registerSync(-1, 'eventRenaudarAutomatizacion.');
    automatizacionDeshabilitada = false;
}

function eventTemperaturaSeleccionada(temperatura){
    if(automatizacionDeshabilitada) return;
    log.registerSync(-1, 'eventTemperaturaSeleccionada: "' + temperatura + '".')
    temperaturaSeleccionada = temperatura;
    evaluarCondicion();
}

function eventCambioSelectorTemperatura(temperatura){
    if(automatizacionDeshabilitada) return;
    log.registerSync(-1, 'eventCambioSelectorTemperatura: "' + temperatura + '".')
    aPantalla.setTemperaturaSeleccionada(temperatura);
}

function eventCambioTemperatura(temperatura){
    if(automatizacionDeshabilitada) return;
    log.registerSync(-1, 'eventCambioTemperatura: "' + temperatura + '".')
    aPantalla.setTemperaturaSensor(temperatura);
    temperaturaSensor = temperatura;
    evaluarCondicion();
}

function eventCambioPresencia(hayPresencia){
    log.registerSync(-1, 'eventCambioPresencia: "' + hayPresencia + '".')
    existePresencia = hayPresencia;
    evaluarCondicion();
}

eventEmitter.on('temperaturaSeleccionadaWeb', eventTemperaturaSeleccionadaWeb);
eventEmitter.on('renaudarAutomatizacion', eventRenaudarAutomatizacion);
eventEmitter.on('temperaturaSeleccionada', eventTemperaturaSeleccionada);
eventEmitter.on('cambioSelectorTemperatura', eventCambioSelectorTemperatura);
eventEmitter.on('cambioTemperatura', eventCambioTemperatura);
eventEmitter.on('cambioPresencia', eventCambioPresencia);