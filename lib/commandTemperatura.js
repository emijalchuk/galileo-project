var eventEmitter = require('./eventEmitter.js')
var sPotenciometro = require('../gpio/sensores/potenciometro.js');

const TEMPERATURA_MINIMA = 10;
const TEMPERATURA_MAXIMA = 30;

var temperaturaSeleccionada = (TEMPERATURA_MAXIMA + TEMPERATURA_MINIMA) / 2;
var automatizacionDeshabilitada = false;
var automatizacionDeshabilitadaSegundos = '-'
var dateTimeAutomatizacionDeshabilitada = '-';

function renaudarAutomatizacionInternal(){
    eventEmitter.emit('renaudarAutomatizacion')
    automatizacionDeshabilitada = false;
    automatizacionDeshabilitadaSegundos = '-';
    dateTimeAutomatizacionDeshabilitada = '-';
    sPotenciometro.setTemperatura(temperaturaSeleccionada);
}

function setTemperaturaSeleccionadaInternal(temperatura, segundos){
    eventEmitter.emit('temperaturaSeleccionadaWeb', temperatura)
    temperaturaSeleccionada = temperatura;
    automatizacionDeshabilitada = true;
    automatizacionDeshabilitadaSegundos = segundos;
    dateTimeAutomatizacionDeshabilitada = new Date();
    setTimeout(function(){
        renaudarAutomatizacionInternal();
    }, segundos * 1000);
}

module.exports = {
    setTemperaturaSeleccionada: function(valor){
        var splited = valor.split('-');
        var temperaturaAux = parseInt(splited[0]);
        var esTemperaturaValida = !isNaN(temperaturaAux) && temperaturaAux >= TEMPERATURA_MINIMA && temperaturaAux <= TEMPERATURA_MAXIMA;
        if(!esTemperaturaValida){
            return;
        }
        var segundosAux = parseInt(splited[1]);
        var sonSegundosValidos = !isNaN(segundosAux) && segundosAux >= 0
        if(!sonSegundosValidos){
            return;
        }
        setTemperaturaSeleccionadaInternal(temperaturaAux, segundosAux);
    },
    renaudarAutomatizacion: function(){
        renaudarAutomatizacionInternal();
    },
    getTemperaturaSeleccionada: function(){
        return temperaturaSeleccionada;
    },
    getAutomatizacionDeshabilitada: function(){
        return automatizacionDeshabilitada;
    },
    getAutomatizacionDeshabilitadaSegundos: function(){
        return automatizacionDeshabilitadaSegundos;
    },
    getDateTimeAutomatizacionDeshabilitada: function(){
        return dateTimeAutomatizacionDeshabilitada;
    }
}