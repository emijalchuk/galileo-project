var http = require('http');
var log = require('./lib/log.js');
var serviceManager = require('./lib/serviceManager.js');
var eventHandler = require('./lib/eventHandler.js');
var web = require('./lib/web.js');
var comando = require('./lib/commandHandler.js');

var numeroRequest = 0;
var PUERTO_DE_ESCUCHA = 8080;

http.createServer(function (request, response) {
    if(request.url === '/favicon.ico') return;
    log.registerSync(numeroRequest, 'Nuevo request: "' + request.url +'"');
    var mode = web.getRequestMode(request);
    log.registerSync(numeroRequest, 'Modo de request: "' + mode + '".');
    var command = web.getRequestCommand(request);
    log.registerSync(numeroRequest, 'Comando recibido: "' + command + '".');
    var commandValue = web.getRequestCommandValue(request);
    log.registerSync(numeroRequest, 'Valor de comando recibido: "' + commandValue + '".');
    comando.evaluar(command, commandValue);
    log.registerSync(numeroRequest, 'Comando analizado con exito.');
    web.writeResponse(response, mode);
    log.registerSync(numeroRequest, 'Response enviado con exito.');
    numeroRequest++;
}).listen(PUERTO_DE_ESCUCHA);
log.registerSync(numeroRequest, 'Servicio lanzado en puerto: ' + PUERTO_DE_ESCUCHA + '.');
serviceManager.iniciarlizarServicio();