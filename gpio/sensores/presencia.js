var tieneLibreriaMraa = true;
try{
    var m = require('mraa');
    var sPIR = new m.Gpio(9); // Pin Central.
    sPIR.dir(m.DIR_IN);
}catch(e){
    tieneLibreriaMraa = false;
}
var eventEmitter = require('../../lib/eventEmitter.js');

const INTERVALO_ACTUALIZACION_SEGUNDOS = 1;
const INTERVALO_EVALUACION_PRESENCIA_SEGUNDOS = 15;

var hayPresencia = false;
var hayPresenciaAnt = false;
var esInicioSistema = true;
var arrPresencias = [];
var cantCiclos = 0;

eventEmitter.emit('cambioPresencia', hayPresencia);

if(tieneLibreriaMraa){
    setInterval(function() {
        var valor = sPIR.read();
        arrPresencias.push(valor);
        if(cantCiclos === INTERVALO_EVALUACION_PRESENCIA_SEGUNDOS || esInicioSistema){
            esInicioSistema = false;
            cantCiclos = 0;
            hayPresencia = determinarSiExistePresencia(arrPresencias);
            arrPresencias = [];
            if(hayPresencia !== hayPresenciaAnt){
                hayPresenciaAnt = hayPresencia;
                eventEmitter.emit('cambioPresencia', hayPresencia);
            }
        }
        cantCiclos++;
    }, INTERVALO_ACTUALIZACION_SEGUNDOS * 1000);
}

function determinarSiExistePresencia(presenciasArray){
    for(var i = 0; i < presenciasArray.length; i++){
        if(presenciasArray[i] === 1){
            return true;
        }
    }
    return false;
}

module.exports = {
    getEstado: function(){
        return hayPresencia;
    }
};
