var tieneLibreriaMraa = true;
try{
    var m = require('mraa');
    var sPotenciometro = new m.Gpio(4); // Pin CLK.
    sPotenciometro.dir(m.DIR_IN);
}catch(e){
    tieneLibreriaMraa = false;
}
var eventEmitter = require('../../lib/eventEmitter.js');

const TEMPERATURA_MINIMA = 10;
const TEMPERATURA_MAXIMA = 30;
var temperatura = tieneLibreriaMraa ? TEMPERATURA_MINIMA : 99;
var antValor = 0;

var cantidadCiclos = 0;
const INTERVALO_ACTUALIZACION_SEGUNDOS = 0.1;
const INTERVALO_PARA_TEMPERATURA_ELEGIDA_SEGUNDOS = 1.5;

eventEmitter.emit('cambioSelectorTemperatura', temperatura);
eventEmitter.emit('temperaturaSeleccionada', temperatura);

if(tieneLibreriaMraa){
    setInterval(function() {
        var valor = sPotenciometro.read();
        if(valor !== antValor){
            antValor = valor;
            temperatura++;
            if(temperatura === TEMPERATURA_MAXIMA + 1){
                temperatura = TEMPERATURA_MINIMA;
            }
            cantidadCiclos = 0;
            eventEmitter.emit('cambioSelectorTemperatura', temperatura);
        }
        var temperaturaSeleccionada = (cantidadCiclos * INTERVALO_ACTUALIZACION_SEGUNDOS) / INTERVALO_PARA_TEMPERATURA_ELEGIDA_SEGUNDOS === 1;
        if(temperaturaSeleccionada){
            eventEmitter.emit('temperaturaSeleccionada', temperatura);
        }
        cantidadCiclos++;
    }, INTERVALO_ACTUALIZACION_SEGUNDOS * 1000)
}

module.exports = {
    setTemperatura: function(valor){
        temperatura = valor;
    },
    getTemperatura: function(){
        return temperatura;
    }
};