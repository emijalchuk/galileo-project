var tieneLibreriaMraa = true;
try{
    var m = require('mraa');
    var sTemperatura = new m.Aio(0);
}catch(e){
    tieneLibreriaMraa = false;
}
var eventEmitter = require('../../lib/eventEmitter.js');

/*
function valueToCentigradosSensorArduinoSoa(temperaturaSensor){
    var resultadoEnCentigrados = 1/(Math.log((1023/temperaturaSensor-1)*100000/100000)/4275+1/298.15)-273.15;
    return resultadoEnCentigrados.toFixed(2);
}
*/

function valueToCentigrados(temperaturaSensor){
    var resultadoEnCentigrados = ((temperaturaSensor*0.004882814)-0.5)*100;
    return Math.round(resultadoEnCentigrados);
}

var temperatura = 99;
var antTemperatura = 0;
const TEMPERATURE_REFRESH_TIME_SECONDS = 10;

eventEmitter.emit('cambioTemperatura', temperatura);

if(tieneLibreriaMraa){
    var temperaturaSensor = sTemperatura.read();
    temperatura = valueToCentigrados(temperaturaSensor);
    eventEmitter.emit('cambioTemperatura', temperatura);
    setInterval(function() {
        var temperaturaSensor = sTemperatura.read();
        temperatura = valueToCentigrados(temperaturaSensor);
        if(temperatura !== antTemperatura){
            antTemperatura = temperatura;
            eventEmitter.emit('cambioTemperatura', temperatura);
        }
    }, TEMPERATURE_REFRESH_TIME_SECONDS * 1000)
}

module.exports = {
    getTemperatura: function(){
        return temperatura;
    }
};
