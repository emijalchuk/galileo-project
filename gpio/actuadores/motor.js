module.exports = Start;

function Start(pinNumber){
    var tieneLibreriaMraa = true;
    try{
        var m = require('mraa');
        var aMotor = new m.Gpio(pinNumber);
        aMotor.dir(m.DIR_OUT);
    }catch(e){
        tieneLibreriaMraa = false;
    }

    var estaEncendido = false;

    function setEstado(encenderMotor){
        if(tieneLibreriaMraa){
            aMotor.write(encenderMotor ? 1 : 0);
        }
        estaEncendido = encenderMotor;
    }

    return {
        getEstado: function(){
            return estaEncendido;
        },
        encender: function(){
            setEstado(true);
        },
        apagar: function(){
            setEstado(false);
        }
    }
};
