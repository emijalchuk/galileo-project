var tieneLibreriaMraa = true;
try{
    var m = require('mraa');
    var aLedGalileo = new m.Gpio(13);
    aLedGalileo.dir(m.DIR_OUT);
}catch(e){
    tieneLibreriaMraa = false;
}

var estaEncendido = false;

function setEstado(encenderLed){
    if(tieneLibreriaMraa){
        aLedGalileo.write(encenderLed ? 1 : 0);
    }
    estaEncendido = encenderLed;
}

module.exports = {
    getEstado: function(){
        return estaEncendido;
    },
    encender: function(){
        setEstado(true);
    },
    apagar: function(){
        setEstado(false);
    }
};
