var tieneLibreriaMraa = true;
try{
    var tm1637 = require('jsupm_tm1637');
    var aPantalla = new tm1637.TM1637(0, 1); //Pin CLK = 0; Pin DIO = 1;
}catch(e){
    tieneLibreriaMraa = false;
}
var eventEmitter = require('../../lib/eventEmitter.js');

// Flags 7 segementos.
//      -A-
//   -F-   -B-
//      -G-
//   -E-   -C-
//      -D-   -DP-
// (se traducen en 8 bits, siendo el 1 el flag que se active) 00000000

const A = 0x77;
const B = 0x7f;
const C = 0x39;
const D = 0x3f;
const E = 0x79;
const F = 0x71;
const G = 0x6f;
const H = 0x76;
const Iizq = 0x30;
const Ider = 0x06;
const J = 0x1f;
const K = 0x00; //NOT DEFINED.
const L = 0x38;
const M = 0x00; //NOT DEFINED.
const N = 0x54;
const O = 0x3f;
const P = 0x73;
const Q = 0x67;
const R = 0x50;
const S = 0x6d;
const Tizq = 0x31;
const Tder = 0x07;
const U = 0x3e;
const V = 0x1c;
const W = 0x00; //NOT DEFINED.
const X = 0x00; //NOT DEFINED.
const Y = 0x6e;
const Z = 0x5b;
const EMPTY = 0x00; //NOT DEFINED.

var temperaturaSeleccionada = "99";
var temperaturaSensor = "99";
var seDioMensajeWelcome = false;
const WELCOME_MESSAGE_TIME_SECONDS = 3;

if(tieneLibreriaMraa){
    // aPantalla.writeString("HOLA"); Toma la letra 'o' (chiquita).
    aPantalla.write(H,O,L,A);
    aPantalla.setColon(false);

    setTimeout(function(){
        seDioMensajeWelcome = true;
    }, WELCOME_MESSAGE_TIME_SECONDS * 1000);
}

function writeTemperatureInScreen(){
    if(seDioMensajeWelcome && tieneLibreriaMraa){
        var temperaturas = temperaturaSensor.toString() + temperaturaSeleccionada.toString();
        aPantalla.writeString(temperaturas);
        aPantalla.setColon(true);
    }
}

function formatTemperature(temperatura){
    var temperaturaFormateada = temperatura.toString();
    if(temperaturaFormateada.length < 2){
        temperaturaFormateada = '0' + temperaturaFormateada;
    }
    return temperaturaFormateada;
}

module.exports = {
    getTemperaturas: function(){
        return temperaturaSeleccionada + ':' + temperaturaSensor;
    },
    setTemperaturaSeleccionada: function(temperatura){
        temperaturaSeleccionada = formatTemperature(temperatura);
        writeTemperatureInScreen();
    },
    setTemperaturaSensor: function(temperatura){
        temperaturaSensor = formatTemperature(temperatura);
        writeTemperatureInScreen();
    },
    borrar: function(){
        if(tieneLibreriaMraa){
            aPantalla.write(EMPTY, EMPTY, EMPTY, EMPTY);
            aPantalla.setColon(false);
        }
    },
    cerrar: function(){
        if(tieneLibreriaMraa){
            aPantalla.write(C,H,A,U);
            aPantalla.setColon(false);
        }
    }
};
