module.exports = Start;

function Start(pinNumber){
    var tieneLibreriaMraa = true;
    try{
        var m = require('mraa');
        var aLed = new m.Gpio(pinNumber);
        aLed.dir(m.DIR_OUT);
    }catch(e){
        tieneLibreriaMraa = false;
    }

    var estaEncendido = false;

    function setEstado(encenderLed){
        if(tieneLibreriaMraa){
            aLed.write(encenderLed ? 1 : 0);
        }
        estaEncendido = encenderLed;
    }

    return {
        getEstado: function(){
            return estaEncendido;
        },
        encender: function(){
            setEstado(true);
        },
        apagar: function(){
            setEstado(false);
        }
    }
}